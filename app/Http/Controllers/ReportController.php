<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Line;
use App\Objet;
use Yajra\DataTables\DataTables;

class ReportController extends Controller
{
    public function index()
    {
        $lines = me()->entity->objets()->where('type_id', 2)->get();
        return view('reports.index', compact('lines'));
    }

    public function show($report)
    {
        $objet = Objet::whereEntity_id(me()->entity_id)
            ->whereType_id(2)
            ->findorfail($report);
        return view('reports.show', compact('objet'));
    }

    public function datatable(Objet $objet, Request $request)
    {
        $lines = Line::whereObjet_id($objet->id);
        if (!empty($request->from_date)) {
            $lines = $lines->where('start_time', '<', $request->to_date)
                ->where(function ($query) use ($request) {
                    $query->where('end_time', '>', $request->from_date)
                        ->orWhereNull('end_time');
                });
        }
        // else {
        //     $data = $lines;
        // }

        $data = $lines->latest('start_time')->get()->flatten();

        // return Datatables::of($data->latest('start_time'))
        return Datatables::of($data)
            ->addColumn('objet', function ($line) {
                return $line->subobjet->name;
            })
            ->addColumn('startdate', function ($line) {
                return $line->start_time->format('Y-m-d');
                // global $request;
                // return ($line->start_time < $request->from_date) ? $request->from_date : $line->start_time->format('Y-m-d');
            })
            ->addColumn('starttime', function ($line) {
                return $line->start_time->format('H:i');
                // global $request;
                // return ($line->start_time < $request->from_date) ? "00:00" : $line->start_time->format('H:i');
            })
            ->addColumn('enddate', function ($line) {
                return ($line->end_time) ? $line->end_time->format('Y-m-d') : '----';
                // global $request;
                // return ($line->end_time && $line->end_time < $request->to_date) ? $line->end_time->format('Y-m-d') : $request->to_date;
            })
            ->addColumn('endtime', function ($line) {
                return ($line->end_time) ? $line->end_time->format('H:i') : '----';
                // global $request;
                // return ($line->end_time && $line->end_time < $request->to_date)  ? $line->end_time->format('H:i') : "23:59";
            })
            ->addColumn('duration', function ($line) {
                return $line->start_time->diffInMinutes($line->end_time);
                // return $line->start_time->diffForHumans($line->end_time);
            })
            ->tojson();
    }
}

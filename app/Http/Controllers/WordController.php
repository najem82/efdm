<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PhpOffice\PhpWord\TemplateProcessor;
use PhpOffice\PhpWord\IOFactory;

class WordController extends Controller
{
    public function createWordDocument()
    {
        // Template processor instance creation
        $template = new TemplateProcessor(public_path('docs/remp.docx'));

        $data = [
            'date' => '20/10/2020',
            'shift' => '21H00 - 08H00',
            'remplacant' => 'Hadraoui Youness',
            'me' => 'Najem Youness',
            'motif' => 'Personnel'
        ];

        // Variables on different parts of document
        $template->setValues($data);

        // Output the file
        $template->saveAs(public_path('docs/remplacement.docx'));

        return redirect('/docs/remplacement.docx');
    }
}

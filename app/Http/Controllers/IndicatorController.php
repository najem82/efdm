<?php

namespace App\Http\Controllers;

use App\Indicator;
use App\Line;
use App\Objet;
use App\Subobjet;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Yajra\DataTables\Facades\DataTables;

class IndicatorController extends Controller
{
    public function index(Objet $objet)
    {
        $lines = me()->entity->objets()->where('type_id', 2)->get();
        return view('indicators.index', compact('lines'));
    }

    public function show($indicator, Request $request)
    {
        // $line = Objet::whereEntity_id(me()->entity_id)
        //     ->whereType_id(2)
        //     ->findorfail($objet);
        // return $indicator;
        // if ($request) {
        //     return $request->all();
        // }
        $now = Carbon::now()->locale('fr');
        $months = ["janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre"];
        $years = [2020 => 2020, 2019 => 2019];
        $month = $request->month ?? $now->month - 1;
        $year = $request->year ?? $now->year;
        $month_name = $months[$month];
        // $month_name = $month->monthName;
        $title = Objet::findorfail($indicator)->name . " [{$month_name} {$year}]";
        $data = $this->data($indicator, $year, $month + 1);
        $total = array_sum(array_column($data, 'ratio')) / count($data);
        // return $data;
        return view('indicators.show', compact('data', 'title', 'month_name', 'month', 'months', 'years', 'year', 'total'));
    }

    public function totalDuration($subobjet_id, $start_date, $end_date)
    {
        $duration = 0;
        $start_date = Carbon::parse($start_date);
        $end_date = Carbon::parse($end_date);
        $total = $start_date->diffInMinutes($end_date);

        // return $end_date;

        $lines = Line::whereSubobjet_id($subobjet_id)
            ->where('start_time', '<', $end_date)
            ->where(function ($query) use ($start_date) {
                $query->where('end_time', '>', $start_date)
                    ->orWhereNull('end_time');
            });
        $items = $lines->get();
        // return $items;
        foreach ($items as $item) {
            $end_time = ($item->end_time && $item->end_time < $end_date) ? $item->end_time : $end_date;
            $start_time = ($item->start_time > $start_date) ? $item->start_time : $start_date;
            $duration += $start_time->diffInMinutes($end_time);
        }
        return [
            'id' => $subobjet_id,
            'duration' => $duration,
            'total' => $total,
            'ratio' => ($total - $duration) / $total * 100,
            'month' => $start_date->locale('fr')->monthName,
        ];
    }

    public function data($objet, $year = 2020, $month = 7)
    {
        // return $objet;
        $data = [];
        $objets = Subobjet::whereObjet_id($objet)->get();

        $start = Carbon::create($year, $month);
        $end = $start->copy()->endOfMonth();

        foreach ($objets as $item) {
            $raw = $this->totalDuration($item->id, $start, $end);
            $raw['objet'] = $item->name;
            $data[] = $raw;
        }

        // $collection = collect($data);
        // $data = $objet->get()->flatten();

        return $data;
        // return DataTables::of($data)->toJson();
    }
}

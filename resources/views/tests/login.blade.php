<!doctype html>
<html class="default-style" lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="author" content="Najem Uness">
  <link rel="icon" href="favicon.ico">

  <title>Toto</title>

  @include('partials.head')
</head>
<body>
  <style>
    .login-msg {
      height: 46px;
      padding-left: 56px;
      background: url(/img/lock.svg) no-repeat;
    }

    .content {
      background-image: url(/img/airtrafic.png)
    }

  </style>
  <div class="header d-flex justify-content-between align-items-center bg-white p-3 w-100">
    <div class="login-msg">
      <h5 class="font-weight-bold mb-0">Login vers EFDM</h5>
      <span class="small">Entrez vos identifiants ci-dessous</span>
    </div>
    <div class="logo">
      <a href="#"><img src="img/logo.jpg" alt="blue" title="blue"></a>
    </div>
  </div> <!-- header -->
  <div class="content d-flex justify-content-center align-items-center p-5">
    <div class="login-box">
      <div class="card">
        <div class="card-header text-center">
          <strong>Connexion</strong>
        </div>
        <div class="card-body">
          <form action="login.php" method="post">

            <div class="form-group">
              <label for="username">Nom d'utilisateur :</label>
              <input type="text" class="form-control" name="username" autocomplete="off">
            </div>

            <div class="form-group">
              <label for="password">Mot de passe :</label>
              <input type="password" class="form-control" name="password">
            </div>

            <Button class="btn btn-primary" type="submit">Se Connecter</Button>

          </form>
        </div><!-- card body-->
      </div><!-- card -->
    </div>
  </div> <!-- content -->
  <div class="main-footer p-2">
    <div class="container">
      <div class="text-center">
        <p class="mt-2 mb-0"><strong>Copyright &copy; 2020.</strong> Tous les droits sont réservés.</p>
      </div>

    </div><!-- container -->
  </div><!-- footer -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="/js/jquery.slim.min.js"></script>
  <script src="/js/popper.min.js"></script>
  <script src="/js/bootstrap.min.js"></script>

  <!-- Plugins -->
  <script src="/plugins/tagsinput/bootstrap-tagsinput.js"></script><!-- tags input plugin -->
  <script src="/plugins/codemirror/codemirror.js"></script><!-- codemirror plugin -->
  <script src="/plugins/codemirror/codemirror-sql.js"></script><!-- codemirror sql mode -->
  <script src="/plugins/highlight/highlight.pack.js"></script><!-- highlight plugin -->

  <!-- Main Application Scripts -->
  <script src="/js/app.js"></script>
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-140388814-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
      dataLayer.push(arguments);
    }
    gtag('js', new Date());
    gtag('config', 'UA-140388814-1');

  </script>

</body>
</html>

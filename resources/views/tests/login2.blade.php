<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>CodePen - Facebook login form </title>
  <link href='https://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>
  {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css"> --}}
  <link rel="stylesheet" href="/css/AdminLTE.css">
  <link rel="stylesheet" href="/css/login.css">

</head>
<body>



  <div class="header d-flex justify-content-between align-items-center bg-white p-3 w-100">
    <div class="login-msg">
      <h5 class="font-weight-bold mb-0">Login vers EFDM</h5>
      <span class="small">Entrez vos identifiants ci-dessous</span>
    </div>
    <div class="logo">
      <a href="#"><img src="img/logo.jpg" alt="blue" title="blue"></a>
    </div>
  </div> <!-- header -->
  <section class="login-form-wrap">
    <h1>EFDM</h1>
    <form class="login-form" action="POST" action="#">
      <div>
        <input type="email" name="email" required placeholder="Email">
      </div>
      <div>
        <input type="password" name="password" required placeholder="Password">
      </div>
      <input type="submit" value="Login">
    </form>
  </section>
</body>
</html>

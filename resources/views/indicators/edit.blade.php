<x-form title="Update Indicator" :action="route('indicators.update',$indicator)">
  @method('put')
</x-form>

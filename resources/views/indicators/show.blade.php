<x-data :title='$title' subtitle="Facteurs de disponibilité">
  @section('before')
  <div class="row">
    <div class="col-md-6 col-xs-12">
      <form action="" method="post">
        @csrf
        <div class="card">
          <div class="card-body">
            <div class="form-row">
              <div class="form-group col">
                <select class="form-control select2 {{ $errors->has('year') ? 'is-invalid' : '' }}" name="year" id="year" required>
                  @foreach($years as $key => $value)
                  <option value="{{ $key }}" {{ $year == $key ? 'selected' : '' }}>{{ $value }}</option>
                  @endforeach
                </select>
                @if($errors->has('year'))
                <span class="text-danger">{{ $errors->first('year') }}</span>
                @endif
              </div>
              <div class="form-group col">
                <select class="form-control select2 {{ $errors->has('month') ? 'is-invalid' : '' }}" name="month" id="month" required>
                  @foreach($months as $key => $value)
                  <option value="{{ $key }}" {{ $month == $key ? 'selected' : '' }}>{{ $value }}</option>
                  @endforeach
                </select>
                @if($errors->has('month'))
                <span class="text-danger">{{ $errors->first('month') }}</span>
                @endif
              </div>
            </div>
            <button type="submit" class="btn btn-primary btn-block col"><span class="fa fa-play mr-2"></span>Afficher</button>
          </div>
        </div>
      </form>
    </div>
    <div class="col-md-3 col-xs-6 pb-1">
      <div class="small-box bg-danger">
        <div class="inner p-3">
          <h3>{{ $month_name }}</h3>
          <p>{{ $year }}</p>
        </div>
        <div class="icon">
          <i class="fa fa-calendar"></i>
        </div>
      </div>
    </div>
    <div class="col-md-3 col-xs-6">
      <div class="small-box bg-primary">
        <div class="inner p-3">
          <h3>{{ round($total,2) }}<sup style="font-size: 20px">%</sup></h3>
          <p>Valeur totale</p>
        </div>
        <div class="icon">
          <i class="fa fa-chart-pie"></i>
        </div>
      </div>
    </div>
  </div>
  @endsection

  <table class="table table-sm table-striped datatable">
    <thead>
      <tr>
        <th> Mois </th>
        <th> Objet </th>
        <th> Non disponibilité </th>
        <th> Facteurs de disponibilité </th>
      </tr>
    </thead>
    <tbody>
      @foreach($data as $item)
      <tr data-entry-id="{{ $item['id'] }}">
        <td> {{ $item['month'] }} </td>
        <td> {{ $item['objet'] }} </td>
        <td> {{ $item['duration'] }} min </td>
        <td>
          <div class="progress bg-danger">
            <div class="progress-bar bg-success" style="width: {{ $item['ratio'] }}%;">{{ round($item['ratio'],2) }}%</div>
          </div>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>

</x-data>

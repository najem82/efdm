<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  {{-- <link href="http://fonts.googleapis.com/css?family=Droid+Sans:400,700" rel="stylesheet" /> --}}
  <title>Login</title>
</head>
<body>
  <header>
    <div class="login-msg">
      <h5 class="msg">Login vers EFDM</h5>
      <span class="small">Entrez vos identifiants ci-dessous</span>
    </div>
    <div class="logo">
      <a href="/"><img src="img/logo.jpg" alt="blue" title="blue" /></a>
    </div>
  </header>
  <div class="content">
    <div class="card login-form">
      <div class="card-header text-center">
        <strong>Connexion</strong>
      </div>
      <div class="card-body">
        <form action="{{ route('login') }}" method="post">
          @csrf
          <div class="form-group">
            <label for="username">Nom d'utilisateur :</label>
            <input type="text" class="form-control" name="username" autocomplete="off" />
          </div>

          <div class="form-group">
            <label for="password">Mot de passe :</label>
            <input type="password" class="form-control" name="password" />
          </div>

          <button class="btn btn-primary" type="submit">
            Se Connecter
          </button>
        </form>
      </div>
      <!-- card body-->
    </div>
  </div>
  <footer>
    <div>
      <strong>Copyright &copy; 2020.</strong> Tous les droits sont
      réservés.
    </div>
    <div class="float-right d-none d-sm-inline">
      {{ config('app.name')." ".config('app.version') }}
    </div>
  </footer>

  <style>
    /* ---------- RESET ---------- */
    /* ---------- Based upon 'reset.css' in the 
Yahoo! User Interface Library: 
http://developer.yahoo.com/yui ---------- */
    *,
    html,
    body,
    div,
    dl,
    dt,
    dd,
    ul,
    ol,
    li,
    h1,
    h2,
    h3,
    h4,
    h5,
    h6,
    pre,
    form,
    label,
    fieldset,
    input,
    p,
    blockquote,
    th,
    td {
      margin: 0;
      padding: 0;
      box-sizing: border-box;
    }

    table {
      border-collapse: collapse;
      border-spacing: 0;
    }

    fieldset,
    img {
      border: 0;
    }

    address,
    caption,
    cite,
    code,
    dfn,
    em,
    strong,
    th,
    var {
      font-style: normal;
      font-weight: normal;
    }

    caption,
    th {
      text-align: left;
    }

    h1,
    h2,
    h3,
    h4,
    h5,
    h6 {
      font-weight: normal;
    }

    q:before,
    q:after {
      content: "";
    }

    strong {
      font-weight: bold;
    }

    em {
      font-style: italic;
    }

    a img {
      border: none;
    }

    /* Gets rid of IE's blue borders */

    a {
      text-decoration: none;
    }

    body {
      font-size: 1em;
      /* Prevents an IE bug where em's scale out of proportion */
    }

    body,
    form {
      font-family: "Droid Sans", Helvetica, Arial, sans-serif;
      line-height: 1.125em;
      /* 18/16 */
    }

    html {
      height: 100%;
    }

    body {
      min-height: 100%;
      padding: 0;
      margin: 0;
      position: relative;
    }

    header {
      display: flex;
      justify-content: space-between;
      align-items: center;
      padding-left: 1.25em;
      padding-right: 1.25em;
      height: 100px;
      /* background: lightcyan; */
    }

    .content {
      display: flex;
      justify-content: center;
      align-items: center;
      min-height: calc(100vh - 150px);
      /* background: #f8f9fa url("/img/artwork-pattern.png") repeat-x left bottom; */
      background: #f8f9fa url("/img/airtrafic2.jpg") no-repeat;
      background-size: 100%;
      border: 1px solid #eeefef;
      padding: 1.875em 0;
    }

    .login-msg {
      background: url("/img/lock.svg") no-repeat;
      height: 46px;
      padding-left: 56px;
    }

    .login-msg .msg {
      text-transform: uppercase;
      margin: 0.312em 0 0 0;
    }

    .login-msg .small {
      color: #9498a1;
      font-size: 0.625em;
      margin-bottom: 0.625em;
    }

    footer {
      padding: 0 1.25rem 0 1.25rem;
    }

    /* Trick: */
    body {
      position: relative;
    }

    body::after {
      content: "";
      display: block;
      height: 50px;
      /* Set same as footer's height */
    }

    footer {
      position: absolute;
      bottom: 0;
      width: 100%;
      height: 50px;
      display: flex;
      justify-content: space-between;
      align-items: center;
      color: #869099;
      font-size: 0.875rem;
    }

    .login-form {
      min-width: 400px;
      box-shadow: 0 0 30px 1px;
    }

    .card {
      border: 1px solid #d2d2d2;
      /* box-shadow: 0 0 3px rgba(0, 0, 0, 0.07), 0 0 0 rgba(0, 0, 0, 0); */
      border-radius: 0.25rem;
      position: relative;
      display: flex;
      flex-direction: column;
      word-wrap: break-word;
      background-color: #fff;
      background-clip: border-box;
    }

    .card-body {
      flex: 1 1 auto;
      padding: 1.25rem;
    }

    .card-header {
      display: flex;
      background-color: #fafafa;
      padding: 0.5rem 1rem;
      align-items: center;
      justify-content: space-between;
      border-radius: calc(0.25rem - 1px) calc(0.25rem - 1px) 0 0;
      border-bottom: 1px solid rgba(0, 0, 0, 0.125);
    }

    .form-group {
      margin-bottom: 1rem;
    }

    .form-control {
      padding: 0.438rem 0.875rem;
      font-size: 0.894rem;
      line-height: 1.3;
      color: #4e5155;
      border: 1px solid rgba(24, 28, 33, 0.1);
      display: block;
      width: 100%;
      height: calc(1.5em + 0.75rem + 2px);
      font-weight: 400;
      background-color: #fff;
      background-clip: padding-box;
      border-radius: 0.25rem;
      transition: border-color 0.15s ease-in-out,
        box-shadow 0.15s ease-in-out;
    }

    .form-control:focus {
      box-shadow: none;
      border-color: #aaa;
      color: #495057;
      background-color: #fff;
      outline: 0;
    }

    label {
      display: inline-block;
      margin-bottom: calc(0.438rem - 2px) !important;
      font-weight: 500 !important;
      font-size: 0.83125rem !important;
    }

    button {
      background-color: #0060df;
      color: white;
      padding: 0 1.5em;
      /* min-width: 6.3em; */
      min-width: 100%;
      -moz-appearance: none;
      min-height: 32px;
      border: 1px solid transparent;
      border-radius: 2px;
      font-weight: 400;
      text-decoration: none;
      font-size: 1em;
      display: block;
      margin-top: 25px;
    }

    button:hover {
      background-color: #003eaa;
    }

    .text-center {
      text-align: center !important;
    }

  </style>
</body>
</html>
